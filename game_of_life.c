#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#define WORLD_TITLE "[GAME OF LIFE]"
#define LIVE_CELL "○"
#define MAX_ROWS 30
#define MAX_COLS 30

void init_world(void);
void set_initial_config(void);
void life_cycle(void);
void control_population(void);
int count_neighbors(int row, int col);
int neighbor_is_in_range(int neighbor_row, int neighbor_col);
void display_world(void);
void print_boundary(void);
void free_world(void);

int **world;

int main(void)
{
    init_world();

    if (atexit(free_world) != 0) {
        perror("atexit");
        exit(EXIT_FAILURE);
    }

    set_initial_config();
    life_cycle();

    exit(EXIT_SUCCESS);
}

void init_world(void)
{
    int row;

    world = calloc(MAX_ROWS, sizeof *world);
    if (world == NULL) {
        perror("calloc");
        exit(EXIT_FAILURE);
    }

    for (row = 0; row < MAX_ROWS; ++row) {
        world[row] = calloc(MAX_COLS, sizeof **world);
        if (world[row] == NULL) {
            perror("calloc");
            exit(EXIT_FAILURE);
        }
    }
}

void set_initial_config(void)
{
    time_t t;
    int i, repeat;

    srand((unsigned) time(&t));

    repeat = (MAX_ROWS > MAX_COLS) ? MAX_ROWS : MAX_COLS;

    for (i = 0; i < repeat * (repeat / 6); ++i)
        world[rand() % MAX_ROWS][rand() % MAX_COLS] = 1;
}

void life_cycle(void)
{
    while (1) {
        display_world();
        control_population();
        sleep(1);
    }
}

void control_population(void)
{
    int row, col, n_neighbors;

    for (row = 0; row < MAX_ROWS; ++row) {
        for (col = 0; col < MAX_COLS; ++col) {
            n_neighbors = count_neighbors(row, col);
            if (world[row][col]) {
                if (n_neighbors < 2 || n_neighbors > 3)
                    world[row][col] = 0;
            } else {
                if (n_neighbors == 3)
                    world[row][col] = 1;
            }
        }
    }
}

int count_neighbors(int row, int col)
{
    int row_mdf, col_mdf, neighbor_row,
        neighbor_col, n_neighbors = 0;

    for (row_mdf = -1; row_mdf < 2; ++row_mdf) {
        for (col_mdf = -1; col_mdf < 2; ++col_mdf) {
            neighbor_row = row + row_mdf;
            neighbor_col = col + col_mdf;

            if (neighbor_row == row && neighbor_col == col) continue;
            if (neighbor_is_in_range(neighbor_row, neighbor_col)) {
                if (world[neighbor_row][neighbor_col])
                    ++n_neighbors;
            }
        }
    }

    return n_neighbors;
}

int neighbor_is_in_range(int neighbor_row, int neighbor_col)
{
    return (neighbor_row >= 0 && neighbor_row < MAX_ROWS) &&
        (neighbor_col >= 0 && neighbor_col < MAX_COLS);
}

void display_world(void)
{
    int row, col;

    printf("%s\n", WORLD_TITLE);
    print_boundary();
    for (row = 0; row < MAX_ROWS; ++row) {
        for (col = 0; col < MAX_COLS; ++col) {
            if (world[row][col])
                printf(LIVE_CELL);
            else
                printf(" ");
        }
        printf("\n");
    }
    print_boundary();
}

void print_boundary(void)
{
    int col;

    for (col = 0; col < MAX_COLS; ++col)
        printf("-");
    printf("\n");
}

void free_world(void)
{
    int row;

    for (row = 0; row < MAX_ROWS; ++row)
        free(world[row]);

    free(world);
    puts("World destroyed.");
}
