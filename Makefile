CC=gcc
CFLAGS=-ansi -pedantic -Wall
C_FILES=game_of_life.c
EXECUTABLE=game_of_life

all: build

build:
	$(CC) $(CFLAGS) $(C_FILES) -o $(EXECUTABLE)

clean:
	rm -rf $(EXECUTABLE)
